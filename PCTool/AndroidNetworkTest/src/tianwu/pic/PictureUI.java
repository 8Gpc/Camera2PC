package tianwu.pic;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.FileDialog;
import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

public class PictureUI {

	private JFrame frame;
	private Image image = null;
	private Canvas canvas;

	public PictureUI(Image image) {
		this.image = image;
		initialize();
		frame.setVisible(true);
		canvas.update(canvas.getGraphics());
	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 908, 678);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		canvas = new Canvas(){
			private static final long serialVersionUID = 1L;
			public void paint(Graphics g){
				if(image!=null){
					g.drawImage(image, 
							10, 10, 
							image.getWidth(null)/4,image.getHeight(null)/4,
							null);
				}
			}
		};
		frame.getContentPane().add(canvas, BorderLayout.CENTER);
		
		JButton btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				FileDialog fd = new FileDialog(frame,"�洢�ļ�");
				fd.setVisible(true);
				String file = fd.getDirectory()+fd.getFile();
				try {
					ImageIO.write((RenderedImage) image,".jpg", new File(file));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		frame.getContentPane().add(btnSave, BorderLayout.EAST);
	}

}
