package com.example.tianwu.camera2pc;

/**
 * Created by Tianwu on 2015/8/17.
 */
import android.app.Activity;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Tianwu on 2015/8/7.
 */
public class NetworkServer extends Thread {
    private ServerSocket serverSocket;
    private Socket socket;
    private Activity activity;


    public NetworkServer(Activity activity,int port){

        this.activity = activity;
        try {
            serverSocket = new ServerSocket(port);
        }catch (IOException e){

        }
    }
    public void run(){
        try {
            while(true) {
                socket = serverSocket.accept();
                DataInputStream dis = new DataInputStream(socket.getInputStream());
                int index = dis.readInt();
                switch (index) {
                    case 1:
                        ((CameraActivity) activity).takeAPhoto();
                        break;
                    default:
                        dis.close();
                }
            }
        }catch (IOException e){

        }
    }

}
