package com.example.tianwu.camera2pc;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Tianwu on 2015/8/17.
 */
public class NetworkThread extends Thread {
    public static final String TAG = "MyCamera";

    private Socket socket;
    private byte[] data;
    private DataOutputStream dos;
    private String host;
    private int port;

    private int width;
    private int height;
    public NetworkThread(String host,int port){
        this.host = host;
        this.port = port;
    }
    synchronized public void newData(byte[] data,int width,int height){
        this.data = data;
        this.width = width;
        this.height = height;
        this.notify();
    }
    synchronized public void run(){
        try {
            while(true) {
                socket = new Socket(host, port);
                dos = new DataOutputStream(socket.getOutputStream());
                this.wait();
                YuvImage image = new YuvImage(data,ImageFormat.JPEG,width,height,null);
                dos.write(image.getYuvData());
                socket.close();
                Log.d(TAG, "send data.");
            }

        }catch (IOException e){
            Log.d(TAG, e.getMessage());
        }catch (InterruptedException e){
            Log.d(TAG, e.getMessage());
        }
    }
}
